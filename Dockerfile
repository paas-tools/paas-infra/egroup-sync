# oc library requires glibc so we cannot use neither 'scratch' nor 'alpine'
# As the oc CLI is needed, use the openshift-client image
FROM gitlab-registry.cern.ch/paas-tools/openshift-client:latest

# Add a previously built app binary to an empty image
# To build the binary:
COPY main  /

CMD ["/main"]
