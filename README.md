# Overview
Application to synchronize OpenShift groups with LDAP and populate
members from an egroup.

The application has two components:

* A synchronous controller that listens to changes in RoleBindings and triggers e-group
expansion every time a user specifies a new group in a Rolebinding object.
The controller populates a `whitelist.txt` file at startup and then does a initial synchronization with
the existing groups. After that, it starts listening to changes in RoleBinding objects, adding
new occurrences to the `whitelist.txt` and resyncing all groups right after that.
Internal OpenShift groups like `system:...` (all non-internal groups are assumed to correspond to e-groups)
are excluded from the sync.

* A cronjob running every hour that expands already-synced groups in case their content
has changed. The `oc` CLI automatically reuses the list of groups from the last sync done
by the controller, it does not need the `whitelist.txt` file.

Groups are populated from LDAP using the `oc adm groups sync` command.

At the moment, recursive membership is *NOT* supported by this command.

If a group is specified in a RoleBinding but does not map to an existing e-group, nothing happens.

### Deployment
The e-group sync is deployed in both `openshift.cern.ch`, `openshift-dev.cern.ch` and `openshift-test.cern.ch` in the `paas-infra` namespace.
For instructions on how to deploy the namespace, check the [OpenShift docs](https://openshiftdocs.web.cern.ch/openshiftdocs/Deployment/Installing_New_Cluster/9-passInfraNamespace/)


The account running the egroup-sync needs to be able to edit groups, so we are going to assign it the `cluster-admin` role:

```
oc create serviceaccount egroup-sync -n paas-infra
oc adm policy add-cluster-role-to-user cluster-admin -z egroup-sync -n paas-infra
```

Once this is done, just create the application by running:

```
oc create -f deploy/egroup_controller.yaml -n paas-infra
```

### Continuous Integration

The deployment of the application is managed by GitLab-CI. Any changes to the definition and config
files will be automatically redeployed into dev and production when merged to `master`.

Check the [OpenShift docs](https://openshiftdocs.web.cern.ch/openshiftdocs/Deployment/Installing_New_Cluster/9-passInfraNamespace/) for instructions on how to set up the Continuous Integration and Continuous Deployment for this project
