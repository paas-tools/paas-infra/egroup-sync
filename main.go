/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os/exec"
	"sort"
	"strings"
	"time"

	goflag "flag"

	flag "github.com/spf13/pflag"

	"github.com/golang/glog"
	// TODO: Move this to:
	// rbacv1 "k8s.io/api/rbac/v1"
	// when Kubernetes 1.8
	rbacv1 "k8s.io/api/rbac/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
)

// Map containing all listed groups to be synced with LDAP
var syncedGroups map[string]bool

// Dry run mode
var dryRun bool

const (
	ocBinaryPath        = "/bin/oc"
	configYamlFile      = "/etc/egroup-sync/config.yaml"
	whiteListFile       = "/tmp/whitelist.txt"
	groupDnFormatString = "CN=%s,OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
	// Current code is not thread-safe. Only run with runWithSingleThread set to 1.
	runWithSingleThread = 1
)

type Controller struct {
	indexer  cache.Indexer
	queue    workqueue.RateLimitingInterface
	informer cache.Controller
	client   kubernetes.Interface
}

func NewController(queue workqueue.RateLimitingInterface, indexer cache.Indexer, informer cache.Controller, client kubernetes.Interface) *Controller {
	return &Controller{
		informer: informer,
		indexer:  indexer,
		queue:    queue,
		client:   client,
	}
}

func (c *Controller) processNextItem() bool {
	// Wait until there is a new item in the working queue
	key, quit := c.queue.Get()
	if quit {
		return false
	}
	// Tell the queue that we are done with processing this key. This unblocks the key for other workers
	// This allows safe parallel processing because two objects with the same key are never processed in
	// parallel.
	defer c.queue.Done(key)

	// Invoke the method containing the business logic
	err := c.doWork(key.(string))
	// Handle the error if something went wrong during the execution of the business logic
	c.handleErr(err, key)
	return true
}

// doWork is the business logic of the controller. In case an error happened, it has to simply return the error.
// The retry logic should not be part of the business logic.
func (c *Controller) doWork(key string) error {
	obj, exists, err := c.indexer.GetByKey(key)
	if err != nil {
		glog.Errorf("Fetching object with key %s from store failed with %v", key, err)
		return err
	}
	if !exists {
		// If the roleBinding does not exist, skip this
		return nil
	}

	// If the roleBinding contains a new group, trigger a resync
	roleBinding := obj.(*rbacv1.RoleBinding)
	glog.V(5).Infof("RoleBinding: %s", roleBinding.GetName())
	if collectGroupsFromRoleBinding(roleBinding) {
		triggerEgroupSync()
	}

	return nil
}

// handleErr checks if an error happened and makes sure we will retry later.
func (c *Controller) handleErr(err error, key interface{}) {
	if err == nil {
		// Forget about the #AddRateLimited history of the key on every successful synchronization.
		// This ensures that future processing of updates for this key is not delayed because of
		// an outdated error history.
		c.queue.Forget(key)
		return
	}

	// This controller retries 5 times if something goes wrong. After that, it stops trying.
	if c.queue.NumRequeues(key) < 5 {
		glog.Infof("Error syncing object %v: %v", key, err)

		// Re-enqueue the key rate limited. Based on the rate limiter on the
		// queue and the re-enqueue history, the key will be processed later again.
		c.queue.AddRateLimited(key)
		return
	}

	c.queue.Forget(key)
	// Report to an external entity that, even after several retries, we could not successfully process this key
	runtime.HandleError(err)
	glog.Infof("Dropping Object %q out of the queue: %v", key, err)
}

func (c *Controller) Run(threadiness int, stopCh chan struct{}) {
	defer runtime.HandleCrash()

	// Let the workers stop when we are done
	defer c.queue.ShutDown()
	glog.Info("Starting Kubernetes controller")

	go c.informer.Run(stopCh)

	// Wait for all involved caches to be synced, before processing items from the queue is started
	if !cache.WaitForCacheSync(stopCh, c.informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	glog.Info("Stopping Kubernetes controller")
}

func (c *Controller) runWorker() {
	for c.processNextItem() {
	}
}

// Return true if the group looks like an e-group, false if it does not
// (an internal openshift group)
func looksLikeAnEgroup(group string) bool {
	return !strings.ContainsAny(group, ":")
}

// Adds to syncedGroups global map all the grupos in `roleBinding` that look
// like an e-group
func collectGroupsFromRoleBinding(roleBinding *rbacv1.RoleBinding) bool {
	changed := false
	// Check if the roleBinding references any group and if so collect those that like like
	// e-groups but have not been collected
	for _, subject := range roleBinding.Subjects {
		if subject.Kind == rbacv1.GroupKind {
			if _, present := syncedGroups[subject.Name]; !present && looksLikeAnEgroup(subject.Name) {
				glog.Infof("New e-group found: %s", subject.Name)
				syncedGroups[subject.Name] = true
				changed = true
			}
		}
	}
	return changed
}

// If there are egroups to sync, write them into the whitelist file and launch
// an LDAP sync
func triggerEgroupSync() error {
	if len(syncedGroups) > 0 {
		// Sort keys of the goups maps. Also, use the LDAP DN form as expected
		// by `oc adm sync`
		var groups []string
		for groupName := range syncedGroups {
			groups = append(groups, fmt.Sprintf(groupDnFormatString, groupName))
		}
		sort.Strings(groups)

		if err := writeWhiteListFile(groups); err != nil {
			return err
		}
		if err := performLDAPSync(); err != nil {
			return err
		}
	}
	return nil
}

// Write whitelist formed by the LDAP groups to sync in a file. This is needed by the
// LDAP sync mechanism Openshift
func writeWhiteListFile(groups []string) error {
	// ioutil takes care of opening and closing the file, and will always override the content.
	// which is what we want
	if err := ioutil.WriteFile(whiteListFile, []byte(strings.Join(groups, "\n")), 0644); err != nil {
		return err
	}
	glog.Infof("Whitelist file successfully written to %s", whiteListFile)
	return nil
}

// Trigger a LDAP sync by calling `oc adm sync`
func performLDAPSync() error {

	if !dryRun {
		// Locally run `oc adm groups sync` to synchronize groups with LDAP.
		// At the moment, there is no other way to do this.
		cmd := exec.Command(ocBinaryPath, "adm", "groups", "sync", "--sync-config",
			configYamlFile, "--whitelist", whiteListFile, "--confirm")
		var stdout, stderr bytes.Buffer
		cmd.Stdout = &stdout
		cmd.Stderr = &stderr
		if err := cmd.Run(); err != nil {
			glog.Errorf("The sync has finished with errors."+
				"This could be normal if any of the defined groups is not really an LDAP group."+
				"The output of the command was: %s - %s", stdout.String(), stderr.String())
			return nil
		}

	} else {
		glog.Infoln("Synchronization skipped because dryRun is enabled...")
	}
	glog.Infoln("Group LDAP synchronization successfully performed!")
	return nil
}

func main() {
	flag.BoolVar(&dryRun, "dry-run", false, "Set flag to only print the operations instead of actually doing them")
	flag.Parse()

	// Convinces goflags that we have called Parse() to avoid noisy logs.
	// See https://github.com/kubernetes/kubernetes/issues/17162
	goflag.Set("logtostderr", "true")
	goflag.CommandLine.Parse([]string{})

	// creates the connection
	config, err := rest.InClusterConfig()
	if err != nil {
		glog.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatal(err)
	}

	// Initialize global list of groups `syncedGroups`
	syncedGroups = make(map[string]bool)
	// Iterate (not Watch!) over all Namespaces and rolebindings to collect full list of groups and save them into
	// the whitelist

	glog.Infoln("Running initial search for egroups...")
	namespaces, err := clientset.CoreV1().Namespaces().List(metav1.ListOptions{})
	if err != nil {
		glog.Fatal(err)
	}
	for _, namespace := range namespaces.Items {
		// TODO: Use RbacV1() when Kubernetes > 1.7
		localRoleBindings, err := clientset.RbacV1beta1().RoleBindings(namespace.GetName()).List(metav1.ListOptions{})
		if err != nil {
			glog.Error(err)
		}
		for _, roleBinding := range localRoleBindings.Items {
			collectGroupsFromRoleBinding(&roleBinding)
		}
	}
	if err := triggerEgroupSync(); err != nil {
		glog.Fatal(err)
	}

	// TODO: Use RbacV1() when Kubernetes > 1.7
	roleBindingsWatcher := cache.NewListWatchFromClient(clientset.RbacV1beta1().RESTClient(), "roleBindings", "", fields.Everything())

	if dryRun {
		fmt.Println("Running in dry-run mode, operations will not change data.")
	}

	// create the workqueue
	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())

	// Bind the workqueue to a cache with the help of an informer. This way we make sure that
	// whenever the cache is updated, the Object key is added to the workqueue.
	// Note that when we finally process the item from the workqueue, we might see a newer version
	// of the Object than the version which was responsible for triggering the update.
	indexer, informer := cache.NewIndexerInformer(roleBindingsWatcher, &rbacv1.RoleBinding{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(new)
			if err == nil {
				queue.Add(key)
			}
		},
		DeleteFunc: func(obj interface{}) {
			// IndexerInformer uses a delta queue, therefore for deletes we have to use this
			// key function.
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
	}, cache.Indexers{})

	controller := NewController(queue, indexer, informer, clientset)

	// Now let's start the controller
	stop := make(chan struct{})
	defer close(stop)
	go controller.Run(runWithSingleThread, stop)

	// Wait forever
	select {}
}
